package controller;

import model.ConjuntoPecas;
import model.Cor;
import model.Tabuleiro;
import model.pecas.Bispo;
import model.pecas.Cavaleiro;
import model.pecas.Peao;
import model.pecas.Rainha;
import model.pecas.Rei;
import model.pecas.Torre;

public class Jogo {
	private final Tabuleiro tabuleiro;
	
	public Jogo() {
		ConjuntoPecas conjuntoBranco = new ConjuntoPecas();
		ConjuntoPecas conjuntoPreto = new ConjuntoPecas();
		
		tabuleiro = new Tabuleiro(conjuntoBranco, conjuntoPreto);
		
		for(int c = 0; c<tabuleiro.COLUNAS; c++) {
			conjuntoBranco.addPeca(new Peao(tabuleiro.getQuadrado(1, c),Cor.Branco));
		}
		conjuntoBranco.addPeca(new Torre(tabuleiro.getQuadrado(0,0), Cor.Branco));
		conjuntoBranco.addPeca(new Torre(tabuleiro.getQuadrado(0,7), Cor.Branco));
		conjuntoBranco.addPeca(new Cavaleiro(tabuleiro.getQuadrado(0,1), Cor.Branco));
		conjuntoBranco.addPeca(new Cavaleiro(tabuleiro.getQuadrado(0,6), Cor.Branco));
		conjuntoBranco.addPeca(new Bispo(tabuleiro.getQuadrado(0,2), Cor.Branco));
		conjuntoBranco.addPeca(new Bispo(tabuleiro.getQuadrado(0,5), Cor.Branco));
		conjuntoBranco.addPeca(new Rainha(tabuleiro.getQuadrado(0,3), Cor.Branco));
		conjuntoBranco.addPeca(new Rei(tabuleiro.getQuadrado(0,4), Cor.Branco));
		
		for(int c = 0; c<tabuleiro.COLUNAS; c++) {
			conjuntoPreto.addPeca(new Peao(tabuleiro.getQuadrado(6, c),Cor.Preto));
		}
		conjuntoPreto.addPeca(new Torre(tabuleiro.getQuadrado(7,0), Cor.Preto));
		conjuntoPreto.addPeca(new Torre(tabuleiro.getQuadrado(7,7), Cor.Preto));
		conjuntoPreto.addPeca(new Cavaleiro(tabuleiro.getQuadrado(7,1), Cor.Preto));
		conjuntoPreto.addPeca(new Cavaleiro(tabuleiro.getQuadrado(7,6), Cor.Preto));
		conjuntoPreto.addPeca(new Bispo(tabuleiro.getQuadrado(7,2), Cor.Preto));
		conjuntoPreto.addPeca(new Bispo(tabuleiro.getQuadrado(7,5), Cor.Preto));
		conjuntoPreto.addPeca(new Rainha(tabuleiro.getQuadrado(7,3), Cor.Preto));
		conjuntoPreto.addPeca(new Rei(tabuleiro.getQuadrado(7,4), Cor.Preto));
	
	}
	
	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}
}
