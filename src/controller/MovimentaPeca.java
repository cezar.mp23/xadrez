package controller;

import model.Quadrado;
import model.pecas.Peca;

public class MovimentaPeca {

	private Quadrado quadradoInicial, quadradoFinal;
	private Peca peçaEscolhida;
	
	public MovimentaPeca(Quadrado quadradoInicial, Quadrado quadradoFinal, Peca peçaEscolhida) {
		this.quadradoInicial = quadradoInicial;
		this.quadradoFinal = quadradoFinal;
		this.peçaEscolhida = peçaEscolhida;
	}
	
	public Quadrado getQuadradoInicial() {
		return quadradoInicial;
	}

	public void setQuadradoInicial(Quadrado quadradoInicial) {
		this.quadradoInicial = quadradoInicial;
	}

	public Quadrado getQuadradoFinal() {
		return quadradoFinal;
	}

	public void setQuadradoFinal(Quadrado quadradoFinal) {
		this.quadradoFinal = quadradoFinal;
	}

	public Peca getPeçaEscolhida() {
		return peçaEscolhida;
	}

	public void setPeçaEscolhida(Peca peçaEscolhida) {
		this.peçaEscolhida = peçaEscolhida;
	}
}
