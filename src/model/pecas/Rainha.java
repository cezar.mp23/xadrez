package model.pecas;

import java.util.ArrayList;
import java.util.List;

import model.Cor;
import model.Quadrado;
import model.Valor;

public class Rainha extends Peca {

	public Rainha(Quadrado quadrado, Cor cor) {
		super(Valor.rainha, quadrado, cor);
	}
	
	@Override
	public String toString() {
		return this.cor == Cor.Branco ? "Q" : "q";
	}

	@Override
	public List<Quadrado> movimentosPossiveis() {
		List<Quadrado> quadradosPossiveis = new ArrayList<Quadrado>();
		quadradosPossiveis.addAll(verificaMovimentosLineares(1,0));
		quadradosPossiveis.addAll(verificaMovimentosLineares(-1,0));
		quadradosPossiveis.addAll(verificaMovimentosLineares(0,1));
		quadradosPossiveis.addAll(verificaMovimentosLineares(0,1));
		quadradosPossiveis.addAll(verificaMovimentosLineares(1,1));
		quadradosPossiveis.addAll(verificaMovimentosLineares(-1,-1));
		quadradosPossiveis.addAll(verificaMovimentosLineares(-1,1));
		quadradosPossiveis.addAll(verificaMovimentosLineares(1,-1));
		
		return quadradosPossiveis;
	}



}
