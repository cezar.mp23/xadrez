package model.pecas;

import java.util.ArrayList;
import java.util.List;

import model.Cor;
import model.Quadrado;
import model.Valor;

public class Peao extends Peca{

	public Peao(Quadrado quadrado, Cor cor) {
		super(Valor.peao, quadrado, cor);
	}
	@Override
	public String toString() {
		return this.cor == Cor.Branco ? "P" : "p";
	}
	
	@Override
	public List<Quadrado> movimentosPossiveis() {
		return cor == Cor.Branco ?	movimentosPossiveisPecasBrancas() : movimentosPossiveisPecasPretas();

	}
	
	public List<Quadrado> movimentosPossiveisPecasBrancas() {
		List<Quadrado> quadradosPossiveis = new ArrayList<Quadrado>();
		Quadrado quadradoFinal = quadrado.getQuadradosAdjacentes(0, 1);
		if(quadradoFinal != null && quadradoFinal.getPeca() == null) {
			adcMovimento(quadradosPossiveis, quadradoFinal);
			quadradoFinal = quadrado.getQuadradosAdjacentes(0, 2);
			if(quadrado.getLinha() == 1 && quadradoFinal.getPeca() == null) {
				adcMovimento(quadradosPossiveis, quadradoFinal);
			}
		}
		quadradoFinal = quadrado.getQuadradosAdjacentes(1, 1);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(-1, 1);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		return quadradosPossiveis;
		
	}
	public List<Quadrado> movimentosPossiveisPecasPretas() {
		List<Quadrado> quadradosPossiveis = new ArrayList<Quadrado>();
		Quadrado quadradoFinal = quadrado.getQuadradosAdjacentes(0, -1);
		if(quadradoFinal != null && quadradoFinal.getPeca() == null) {
			adcMovimento(quadradosPossiveis, quadradoFinal);
			quadradoFinal = quadrado.getQuadradosAdjacentes(0, -2);
			if(quadrado.getLinha() == 6 && quadradoFinal.getPeca() == null) {
				adcMovimento(quadradosPossiveis, quadradoFinal);
			}
		}
		quadradoFinal = quadrado.getQuadradosAdjacentes(1, -1);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(-1, -1);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		return quadradosPossiveis;
	}

}
