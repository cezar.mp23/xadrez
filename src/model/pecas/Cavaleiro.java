package model.pecas;

import java.util.ArrayList;
import java.util.List;

import model.Cor;
import model.Quadrado;
import model.Valor;

public class Cavaleiro extends Peca {

	public Cavaleiro(Quadrado quadrado, Cor cor) {
		super(Valor.cavaleiro, quadrado, cor);
	}
	@Override
	public String toString() {
		return this.cor == Cor.Branco ? "C" : "c";
	}
	@Override
	public List<Quadrado> movimentosPossiveis() {
		List<Quadrado> quadradosPossiveis = new ArrayList<Quadrado>();
		Quadrado quadradoFinal = quadrado.getQuadradosAdjacentes(1, 2);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(-1, 2);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(1, -2);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(-1, -2);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(2, 1);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(-2, 1);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(2, -1);
		adcMovimento(quadradosPossiveis, quadradoFinal);
		quadradoFinal = quadrado.getQuadradosAdjacentes(-2, -1);
		adcMovimento(quadradosPossiveis, quadradoFinal);

		
		return quadradosPossiveis;
	}



}
