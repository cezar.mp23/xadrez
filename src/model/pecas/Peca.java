package model.pecas;

import java.util.ArrayList;
import java.util.List;
import model.Cor;
import model.Quadrado;
import model.Valor;

public abstract class Peca {
	
	private final Valor valorPeca;
	protected final Cor cor;
	protected Quadrado quadrado;
	
	public abstract List<Quadrado> movimentosPossiveis();
	
	public Peca(Valor valorPeca, Quadrado quadrado, Cor cor) {
		this.valorPeca = valorPeca;
		this.cor = cor;
		this.quadrado = quadrado;
		this.quadrado.setPeca(this);
	}

	public Quadrado getQuadrado() {
		return quadrado;
	}

	public void setQuadrado(Quadrado quadrado) {
		this.quadrado = quadrado;
	}

	public Valor getValorPeca() {
		return valorPeca;
	}

	public Cor getCor() {
		return cor;
	}

	public List<Quadrado> MovimentosPossiveis() {
		// TODO Auto-generated method stub
		return null;
	}
	protected void adcMovimento(List<Quadrado>quadradosPossiveis, Quadrado quadradoAlvo) {
		if(quadradoAlvo != null && (quadradoAlvo.getPeca() == null) || quadradoAlvo.getPeca().cor != cor) {
			quadradosPossiveis.add(quadradoAlvo);
		}	
	}
	protected List<Quadrado> verificaMovimentosLineares(int horizontal, int vertical){
		List<Quadrado>quadradosPossiveis = new ArrayList<Quadrado>();
		Quadrado quadradoFinal = quadrado.getQuadradosAdjacentes(horizontal, vertical);
		
		while(quadradoFinal != null) {
			if(quadradoFinal.getPeca() == null) {
				adcMovimento(quadradosPossiveis, quadradoFinal);

			}
			else if(quadradoFinal.getPeca().cor == cor) {
				break;
			}
			else {
				adcMovimento(quadradosPossiveis, quadradoFinal);
				break;
			}
			quadradoFinal = quadradoFinal.getQuadradosAdjacentes(horizontal, vertical);
		}
		
		
		
		return quadradosPossiveis;
	}
	
}
