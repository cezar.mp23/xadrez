package model.pecas;

import java.util.ArrayList;
import java.util.List;

import model.Cor;
import model.Quadrado;
import model.Valor;

public class Bispo extends Peca {

	public Bispo(Quadrado quadrado, Cor cor) {
		super(Valor.bispo, quadrado, cor);
	}
	@Override
	public String toString() {
		return this.cor == Cor.Branco ? "B" : "b";
	}
	@Override
	public List<Quadrado> movimentosPossiveis() {
		List<Quadrado> quadradosPossiveis = new ArrayList<Quadrado>();
		quadradosPossiveis.addAll(verificaMovimentosLineares(1,1));
		quadradosPossiveis.addAll(verificaMovimentosLineares(-1,-1));
		quadradosPossiveis.addAll(verificaMovimentosLineares(-1,1));
		quadradosPossiveis.addAll(verificaMovimentosLineares(1,-1));

		return quadradosPossiveis;
	}

}
