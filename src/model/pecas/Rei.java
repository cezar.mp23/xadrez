package model.pecas;

import java.util.ArrayList;
import java.util.List;

import model.Cor;
import model.Quadrado;
import model.Valor;

public class Rei extends Peca{

	public Rei(Quadrado quadrado, Cor cor) {
		super(Valor.rei, quadrado, cor);
	}
	
	@Override
	public String toString() {
		return this.cor == Cor.Branco ? "K" : "k";
	}

	@Override
	public List<Quadrado> movimentosPossiveis() {
		Quadrado quadradoFinal;
		List<Quadrado> quadradosPossiveis = new ArrayList<Quadrado>();
		for(int h = -1; h <= 1; h++) {
			for (int v = -1; v <= 1; v++) {
				quadradoFinal = quadrado.getQuadradosAdjacentes(h, v);
				adcMovimento(quadradosPossiveis, quadradoFinal);

			}
		}
		return quadradosPossiveis;
	}

}
