package model;

import java.util.ArrayList;
import java.util.List;
import controller.MovimentaPeca;
import model.pecas.Peca;

public class Jogador {
	private Tabuleiro tabuleiro;
	private Cor cor;
	
	public Jogador(Tabuleiro tabuleiro, Cor cor) {
		super();
		this.tabuleiro = tabuleiro;
		this.cor = cor;
	}
	public MovimentaPeca movimentoFeito() {
		return null;
	}
	
	public void realizarMovimento(MovimentaPeca movimento) {
		if(movimento.getPeçaEscolhida() != null) {
			ConjuntoPecas conjunto = tabuleiro.buscarConjunto(cor.corOposta());
			conjunto.removerPeca(movimento.getPeçaEscolhida());
		}
		Peca pecaEmMovimento = movimento.getQuadradoInicial().getPeca();
		pecaEmMovimento.setQuadrado(movimento.getQuadradoFinal());
		movimento.getQuadradoInicial().setPeca(null);
 	}
	
	public List<MovimentaPeca> LegalizaMovimentos(){
		List<Peca> pecasEmJogo = tabuleiro.buscarConjunto(cor).getPecasEmJogo();
		List<MovimentaPeca> movimentosLegais = new ArrayList<MovimentaPeca>();
	
		for(Peca p: pecasEmJogo) {
			for(Quadrado q : p.movimentosPossiveis()) {
				movimentosLegais.add(new MovimentaPeca(p.getQuadrado(), q, q.getPeca()));
			}
		}
		
		return movimentosLegais;
	}
}
