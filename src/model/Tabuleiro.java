package model;

public class Tabuleiro {
	
	public static final int LINHAS = 8;
	public static final int COLUNAS = 8;
	
	private final Quadrado[][] quadrados;
	private ConjuntoPecas conjuntoBranco;
	private ConjuntoPecas conjuntoPreto;
	
	public Tabuleiro(ConjuntoPecas jogadorBranco, ConjuntoPecas jogadorPreto) {
		quadrados = new Quadrado[LINHAS][COLUNAS];
		this.conjuntoBranco = jogadorBranco;
		this.conjuntoPreto = jogadorPreto;
		
		for(int linha = 0; linha < LINHAS; linha++) {
			for(int coluna = 0; coluna< COLUNAS; coluna++) {
				quadrados[linha][coluna] = new Quadrado(this, linha, coluna);
			}
		}
	}
	
	public ConjuntoPecas buscarConjunto(Cor cor) {
		return cor == Cor.Branco ? conjuntoBranco : conjuntoPreto;
	}
	
	public Quadrado getQuadrado(int linha, int coluna) {
		
		if(linha < 0 || coluna>= COLUNAS || linha >= LINHAS || coluna < 0)
			return null;
		
		
		return quadrados[linha][coluna];
	}
}
