package model;

import java.util.ArrayList;
import java.util.List;

import model.pecas.Peca;

public class ConjuntoPecas {
	private final List<Peca> pecasEmJogo;
	private final List<Peca> pecasPerdidas;
	
	public List<Peca> getPecasEmJogo() {
		return pecasEmJogo;
	}
	public List<Peca> getPecasPerdidas() {
		return pecasPerdidas;
	}
	public ConjuntoPecas() {
		pecasEmJogo = new ArrayList<Peca>();
		pecasPerdidas = new ArrayList<Peca>();
		
	}
	public void addPeca(Peca peca) {
		pecasEmJogo.add(peca);
	}
	public void removerPeca(Peca peca) {
		pecasEmJogo.remove(peca);
		pecasPerdidas.remove(peca);
	}
	
}
