package model;

import model.pecas.Peca;

public class Quadrado {
	
	private final Tabuleiro tabuleiro;
	private final int linha;
	private final int coluna;
	//Não é final pq pode haver mudança da peça no quadrado
	private Peca peca;
	private boolean temPeca;
	
	public Quadrado(Tabuleiro tabuleiro, int linha, int coluna) {
		this.tabuleiro = tabuleiro;
		this.linha = linha;
		this.coluna = coluna;
	}
	public void setPeca(Peca peca) {
		this.peca = peca;
		if(peca != null) {
			setTemPeca(true);
		}else {
			setTemPeca(false);
		}
	}
	
	public Quadrado getQuadradosAdjacentes(int horizontal, int vertical) {
		return tabuleiro.getQuadrado(horizontal + coluna, vertical + linha);
	}
	
	public Peca getPeca() {
		return peca;
	}
	public boolean isTemPeca() {
		return temPeca;
	}
	public void setTemPeca(boolean temPeca) {
		this.temPeca = temPeca;
	}
	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}
	public int getLinha() {
		return linha;
	}
	public int getColuna() {
		return coluna;
	}
		
}
