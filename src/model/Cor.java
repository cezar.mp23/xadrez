package model;

public enum Cor {
	Branco, Preto;
	
	public Cor corOposta() {
		return this == Branco ? Preto : Branco;
	}
}
