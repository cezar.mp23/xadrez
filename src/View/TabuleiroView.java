package View;

import controller.Jogo;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.shape.Rectangle;
import model.Cor;
import model.Tabuleiro;

public class TabuleiroView extends Application {

	private Jogo jogo;
	private IconesPecas i;
    public static void main(String[] args) {
        launch(args);
    }

	@Override
	public void start(Stage stage) throws Exception {
		GridPane pane = new GridPane();
		i = new IconesPecas(this);
		for(int linha = 0; linha < Tabuleiro.LINHAS; linha++) {
			for(int coluna = 0; coluna < Tabuleiro.COLUNAS; coluna++) {
				StackPane quadrado = new StackPane();
				String cor;
				if((linha + coluna) % 2 == 0) {
					cor = "white";
				}else {
					cor = "black";
				}
				quadrado.setStyle("-fx-background-color: "+cor+";");
				pane.add(quadrado, linha,coluna);
			}
		}
		
		for (int i = 0; i < 8; i++) {
            pane.getColumnConstraints().add(new ColumnConstraints(5, Control.USE_COMPUTED_SIZE, Double.POSITIVE_INFINITY, Priority.ALWAYS, HPos.CENTER, true));
            pane.getRowConstraints().add(new RowConstraints(5, Control.USE_COMPUTED_SIZE, Double.POSITIVE_INFINITY, Priority.ALWAYS, VPos.CENTER, true));
        }
		Scene scene = new Scene(pane,800,800);
		stage.setScene(scene);
		//stage.setResizable(false);
	    ObservableList<Node> childrens = pane.getChildren();
	    i.atualizaIcones(pane);
		stage.setTitle("Xadrez Radical");
		stage.show();
	}
	
	public void inicializaVariaveis() {
		jogo = new Jogo();
	}
}