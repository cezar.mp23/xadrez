package View;

import java.util.ArrayList;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import model.ConjuntoPecas;
import model.Cor;
import model.Tabuleiro;

public class IconesPecas {
	private TabuleiroView tabuleiroView;
	private Tabuleiro tabuleiro;
	private final String caminho = "/home/marcos/Área de trabalho/POO/xadrez/Sprites/";
	public IconesPecas(TabuleiroView tabuleiro) {
		this.tabuleiroView = tabuleiro;
	}
	public void atualizaIcones(GridPane pane) {
	    ObservableList<Node> childrens = pane.getChildrenUnmodifiable();
	    
	    StackPane centeredNode = (StackPane) getCenteredNodeGridPane(pane, 0, 0);
	    StackPane centeredNode1 = (StackPane) getCenteredNodeGridPane(pane, 0, 1);
	    ImageView img = new ImageView(new Image("blackBishop.png"));
	    img.setPreserveRatio(true);

	    img.fitWidthProperty().bind(centeredNode.widthProperty());
	    img.fitHeightProperty().bind(centeredNode.heightProperty());
	    centeredNode.setStyle("-fx-background-image: url('blackBishop.png');-fx-background-size: cover;");
	    

	}
	
	private StackPane getCenteredNodeGridPane(GridPane gridPane, int col, int row) {
	    for (Node node : gridPane.getChildren()) {
	        if (node instanceof StackPane 
	         && GridPane.getColumnIndex(node) == col/2 
	         && GridPane.getRowIndex(node) == row/2) {
	            return (StackPane) node;
	        }
	    }
	    return null;
	}
}
